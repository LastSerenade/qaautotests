import java.util.Scanner;

public class square{

public static void main(String args[]){

	Scanner scan = new Scanner(System.in);
	double a, b, c;
	double D;
	double x1, x2;
	double x;
	System.out.println("Enter a: ");
	a = scan.nextDouble();
	System.out.println("Enter b: ");
	b = scan.nextDouble();
	System.out.println("Enter c: ");
	c = scan.nextDouble();
	D = Math.pow(b, 2) - 4 * a * c;
	if (D > 0){
		x1 = (-b - Math.sqrt(D)) / (2 * a);
		x2 = (-b + Math.sqrt(D)) / (2 * a);
    	System.out.println("Корни квадратного уравнения: x1 = " + x1 + "  x2 =" + x2);	
	}
	else 
		if (D == 0){
		x = -b / (2 * a);
		System.out.println("x1, x2 =" + x);
	}
	else 
		if (D < 0){
		System.out.println("Квадратное уравнение не имеет корней");
	}
}
}