package pom.src;

import java.util.concurrent.TimeUnit;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.ErrorCodes;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class googlecomposemail{

	public static WebDriver driver;

	@BeforeClass()
	public static void setup() {
		
		System.setProperty("webdriver.gecko.driver", "C:/Users/Satan/drivers/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://gmail.com/");
	}

	@Test
	public void composeMail() throws Exception {
		
		WebElement emailField = driver.findElement(By.id("identifierId"));
		emailField.sendKeys("qaclub@gmail.com");
		WebElement nextButton = driver.findElement(By.xpath("//span[@class='RveJvd snByac']"));
		nextButton.click();
		
		
		// Пытался привязать wait.until для поля password, но не вышло. Поэтому рискнул пальцами. ) 
		//WebElement waitForPass = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='aoP aoC']")));
		
		Thread.sleep(2000);
		//
		WebElement inputPass = driver.findElement(By.xpath("//input[@class='whsOnd zHQkBf']"));
		inputPass.sendKeys("test");
		
		WebElement signInButton = driver.findElement(By.xpath("//span[@class='RveJvd snByac']"));
		signInButton.click();
		WebElement composeButton = driver.findElement(By.xpath("//div[@class='T-I J-J5-Ji T-I-KE L3']"));
		composeButton.click();
		
		WebElement focusToArea = driver.findElement(By.xpath("//textarea[@class='vO']"));
		
		focusToArea.sendKeys("arthur.zalata@gmail.com");
		WebElement focusTextArea = driver.findElement(By.xpath("//div[@class='Am Al editable LW-avf']"));
		focusTextArea.sendKeys("Some text for Arthur: Say hello to QA club");
		WebElement sendButton = driver.findElement(By.xpath("//div[@class='T-I J-J5-Ji aoO T-I-atl L3']"));
		sendButton.click();
		
		Thread.sleep(2000);
		
		driver.navigate().refresh();
		
		
		WebElement checkNewMail = driver.findElement(By.xpath("//tr[@jsmodel='nXDxbd'][1]"));
		checkNewMail.click();
		WebElement checkMessage = driver.findElement(By.xpath("//div[@dir='ltr' and contains(text(), 'Some text for Arthur: Say hello to QA club')]"));
		String checkMail = checkMessage.getText();
		Assert.assertEquals("Some text for Arthur: Say hello to QA club", checkMail);
	}
	//@AfterClass
	//public static void tearDown() {
	//	driver.quit();
	//}
}
