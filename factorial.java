import java.util.Scanner;

public class Factorial{

	public static void main(String args[]){

		int n;
		do {

			Scanner scan = new Scanner(System.in);
			System.out.print("Введи целое число n : ");
			n = scan.nextInt();
			int result = 1;
			for (int i = 2; i <= n; i++)
				result *= i;
			System.out.println(result);
			if (n < 0)
				System.out.println("Отрицательное число");
		} 

		while (n < 0); {

			System.out.println("Готово");
		}
	} 
} 
